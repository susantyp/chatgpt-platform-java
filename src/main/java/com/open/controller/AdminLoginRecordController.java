package com.open.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.open.bean.LoginRecord;
import com.open.result.OpenResult;
import com.open.service.LoginRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@RestController
@RequestMapping("/request/open")
@Slf4j
public class AdminLoginRecordController {

    @Autowired
    LoginRecordService loginRecordService;


    @GetMapping("/admin/login/record")
    public OpenResult loginRecord(@RequestParam Integer current,
                                  @RequestParam Integer size,
                                  @RequestParam(required = false) String startTime,
                                  @RequestParam(required = false) String endTime){

        IPage<LoginRecord> page = loginRecordService.page(new Page(current,size),new QueryWrapper<LoginRecord>()
                .between(StrUtil.isNotBlank(startTime) && StrUtil.isNotBlank(endTime), "login_time", startTime, endTime)
                .orderByDesc("lrid"));
        return OpenResult.success(page);
    }

}
