package com.open.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.open.bean.LoginRecord;
import com.open.bean.UserUpgradation;
import com.open.result.OpenResult;
import com.open.service.LoginRecordService;
import com.open.service.UserUpgradationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@RestController
@RequestMapping("/request/open")
@Slf4j
public class AdminUserUpgradationController {


    @Autowired
    UserUpgradationService userUpgradationService;


    @GetMapping("/admin/user/upgradation")
    public OpenResult upgradation(@RequestParam Integer current,
                                  @RequestParam Integer size,
                                  @RequestParam(required = false) String startTime,
                                  @RequestParam(required = false) String endTime){

        IPage<UserUpgradation> page = userUpgradationService.page(new Page(current,size),new QueryWrapper<UserUpgradation>()
                .between(StrUtil.isNotBlank(startTime) && StrUtil.isNotBlank(endTime), "create_time", startTime, endTime)
                .orderByDesc("upid"));
        return OpenResult.success(page);
    }

}
