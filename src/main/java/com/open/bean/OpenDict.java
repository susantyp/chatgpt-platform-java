package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName open_dict
 */
@TableName(value ="open_dict")
@Data
public class OpenDict implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer did;

    /**
     * code编码
     */
    private String dictCode;

    /**
     * 描述文字
     */
    private String dictDes;

    /**
     * 值
     */
    private String dictValue;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 备注
     */
    private String remark;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}