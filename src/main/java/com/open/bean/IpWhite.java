package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName ip_white
 */
@TableName(value ="ip_white")
@Data
public class IpWhite implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer wid;

    /**
     * 白名单ip地址
     */
    private String ip;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 限制当日访问次数
     */
    private Integer requestCount;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * （0=正常，1=封禁，3=待审核）
     */
    private Integer ipStatus;

    /**
     * 绑定的请求机器id
     */
    private Integer tid;

    /**
     * 根据ip转换得到的昵称
     */
    private String nikeName;

    /**
     * 绑定的邮箱(登录账号)
     */
    private String email;

    /**
     * 用户类型0=普通用户，1=内测用户，2=plus用户
     */
    private Integer userType;

    /**
     * 绑定的邮箱(登录密码)
     */
    private String userPassword;

    /**
     * 图片的请求次数
     */
    private Integer requestImgCount;

    /**
     * 上下文绑定的token
     */
    private String messageToken;

    /**
     * 头像
     */
    private String headImg;

    /**
     * 当前用户重置的数量
     */
    private Integer resettingCount;

    /**
     * 第三方用户唯一id
     */
    private String socialUid;

    /**
     * 限制当前用户今日请求数量
     */
    private Integer limitationCount;

    /**
     * 请求绘画的app_key
     */
    private String appKey;

    /**
     * 是否展示个人作画，0=否，1=是
     */
    private Integer isImgShow;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}