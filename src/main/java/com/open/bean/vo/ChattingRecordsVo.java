package com.open.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChattingRecordsVo {

    /**
     * 请求的话语
     */
    private String requestContent;

    /**
     * 请求的时间
     */
    private Date requestTime;

    /**
     * 机器人回复的话术
     */
    private String respondContent;

    /**
     * 机器人回应的时间
     */
    private Date respondTime;

}
