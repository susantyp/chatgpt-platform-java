package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 登录记录
 * @TableName login_record
 */
@TableName(value ="login_record")
@Data
public class LoginRecord implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer lrid;

    /**
     * 登录人
     */
    private Integer wid;

    /**
     * 登录账号
     */
    private String username;

    /**
     * 登录头像
     */
    private String headImg;

    /**
     * 登录时间
     */
    private Date loginTime;

    /**
     * 登录ip
     */
    private String ip;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}