package com.open.mapper;

import com.open.bean.DrawImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【draw_img】的数据库操作Mapper
* @createDate 2023-06-07 11:26:07
* @Entity com.open.bean.DrawImg
*/
public interface DrawImgMapper extends BaseMapper<DrawImg> {

}




