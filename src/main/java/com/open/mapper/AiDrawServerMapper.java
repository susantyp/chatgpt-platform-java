package com.open.mapper;

import com.open.bean.AiDrawServer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【ai_draw_server】的数据库操作Mapper
* @createDate 2023-06-07 11:26:07
* @Entity com.open.bean.AiDrawServer
*/
public interface AiDrawServerMapper extends BaseMapper<AiDrawServer> {

}




