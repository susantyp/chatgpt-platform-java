package com.open.mapper;

import com.open.bean.OpenDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【open_dict】的数据库操作Mapper
* @createDate 2023-06-07 11:26:07
* @Entity com.open.bean.OpenDict
*/
public interface OpenDictMapper extends BaseMapper<OpenDict> {

}




