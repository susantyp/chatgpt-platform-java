package com.open;

import com.open.source.DynamicDataSourceConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
@Import({DynamicDataSourceConfig.class})
@MapperScan(basePackages = "com.open.mapper")
@EnableScheduling
@EnableAsync
public class ChatGptApplication {


    public static void main(String[] args) {
        SpringApplication.run(ChatGptApplication.class, args);
    }
}
