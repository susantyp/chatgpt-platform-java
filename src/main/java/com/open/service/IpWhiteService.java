package com.open.service;

import com.open.bean.IpWhite;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【ip_white】的数据库操作Service
* @createDate 2023-06-07 11:26:07
*/
public interface IpWhiteService extends IService<IpWhite> {

}
