package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.AiDrawServer;
import com.open.service.AiDrawServerService;
import com.open.mapper.AiDrawServerMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【ai_draw_server】的数据库操作Service实现
* @createDate 2023-06-07 11:26:07
*/
@Service
public class AiDrawServerServiceImpl extends ServiceImpl<AiDrawServerMapper, AiDrawServer>
    implements AiDrawServerService{

}




