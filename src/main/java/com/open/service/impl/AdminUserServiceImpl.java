package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.AdminUser;
import com.open.service.AdminUserService;
import com.open.mapper.AdminUserMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【admin_user】的数据库操作Service实现
* @createDate 2023-06-07 11:26:07
*/
@Service
public class AdminUserServiceImpl extends ServiceImpl<AdminUserMapper, AdminUser>
    implements AdminUserService{

}




