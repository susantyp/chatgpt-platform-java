package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.RequestRecord;
import com.open.service.RequestRecordService;
import com.open.mapper.RequestRecordMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【request_record】的数据库操作Service实现
* @createDate 2023-06-07 11:26:08
*/
@Service
public class RequestRecordServiceImpl extends ServiceImpl<RequestRecordMapper, RequestRecord>
    implements RequestRecordService{

}




