package com.open.service.impl;

import cn.hutool.core.util.StrUtil;
import com.open.bean.vo.ReportVo;
import com.open.bean.vo.date.DayVo;
import com.open.bean.vo.date.HourVo;
import com.open.bean.vo.date.MonthVo;
import com.open.mapper.AdminUserMapper;
import com.open.mapper.IpWhiteMapper;
import com.open.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    AdminUserMapper adminUserMapper;

    @Override
    public ReportVo queryHour(String dateStr, Integer value, String tableName,String timeName) throws IllegalAccessException {
        String[] split = dateStr.split("-");
        String unit = "count";
        ReportVo reportVo = new ReportVo();
        HourVo hourVo = adminUserMapper.queryHour(split[0], split[1], split[2],tableName,unit,timeName);
        reportVo.setHourVos(hourVo);
        return result(hourVo.getClass(),hourVo,reportVo);
    }

    @Override
    public ReportVo queryDay(String dateStr, Integer value, String tableName,String timeName) throws IllegalAccessException {
        String[] split = dateStr.split("-");
        String unit = "count";
        ReportVo reportVo = new ReportVo();
        DayVo dayVo = adminUserMapper.queryDay(split[0], split[1], split[2],tableName,unit,timeName);
        reportVo.setDayVos(dayVo);
        return result(dayVo.getClass(),dayVo,reportVo);
    }

    @Override
    public ReportVo queryMonth(String dateStr, Integer value, String tableName,String timeName) throws IllegalAccessException {
        String[] split = dateStr.split("-");
        String unit = "count";
        ReportVo reportVo = new ReportVo();
        MonthVo monthVo = adminUserMapper.queryMonth(split[0], split[1], split[2],tableName,unit,timeName);
        reportVo.setMonthVos(monthVo);
        return result(monthVo.getClass(),monthVo,reportVo);
    }

    public ReportVo result(Class<?> cls,Object obj,ReportVo reportVo) throws IllegalAccessException {
        List<Integer> integerList = new ArrayList<>();
        for (Field field : cls.getDeclaredFields()) {
            field.setAccessible(true);
            String val = field.get(obj).toString();
            if (StrUtil.isBlank(val)){
                integerList.add(0);
            }else {
                integerList.add(Integer.parseInt(val.trim()));
            }
        }
        reportVo.setData(integerList);
        return reportVo;
    }
}
