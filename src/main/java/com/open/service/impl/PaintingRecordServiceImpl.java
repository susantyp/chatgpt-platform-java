package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.PaintingRecord;
import com.open.service.PaintingRecordService;
import com.open.mapper.PaintingRecordMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【painting_record】的数据库操作Service实现
* @createDate 2023-06-07 11:26:08
*/
@Service
public class PaintingRecordServiceImpl extends ServiceImpl<PaintingRecordMapper, PaintingRecord>
    implements PaintingRecordService{

}




