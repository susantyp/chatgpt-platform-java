package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.VefSign;
import com.open.service.VefSignService;
import com.open.mapper.VefSignMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【vef_sign】的数据库操作Service实现
* @createDate 2023-06-07 11:26:08
*/
@Service
public class VefSignServiceImpl extends ServiceImpl<VefSignMapper, VefSign>
    implements VefSignService{

}




