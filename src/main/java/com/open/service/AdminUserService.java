package com.open.service;

import com.open.bean.AdminUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【admin_user】的数据库操作Service
* @createDate 2023-06-07 11:26:07
*/
public interface AdminUserService extends IService<AdminUser> {

}
